from pyspark.sql import SparkSession
import mysql.connector
from pyspark.sql.functions import col, udf, lit
from pyspark.sql.types import BooleanType

OPENFOODFACTS_CSV_PATH = "https://fr.openfoodfacts.org/data"
DIET_PLANS_CSV_PATH = "csv/PlansDiet.csv"
USERS_CSV_PATH = "csv/Users.csv"

def clean_data(openfoodfacts_df, diet_plans_df, users_df):
    openfoodfacts_df = openfoodfacts_df.filter(
        col("energy_100g").isNotNull() &
        col("proteins_100g").isNotNull() &
        col("fat_100g").isNotNull() &
        col("carbohydrates_100g").isNotNull()
    )

    openfoodfacts_df = openfoodfacts_df.filter(
        (col("energy_100g") > 0) &
        (col("proteins_100g") > 0) &
        (col("fat_100g") > 0) &
        (col("carbohydrates_100g") > 0)
    )

    joined_df = openfoodfacts_df.join(
        users_df, openfoodfacts_df["countries_en"] == users_df["country"], "inner")
    openfoodfacts_df = joined_df.filter(
        (col("product_name").isNotNull()) &
        (col("countries_en").isNotNull())
    )

    openfoodfacts_df = openfoodfacts_df.dropDuplicates(["product_name", "countries_en"])

    openfoodfacts_df = openfoodfacts_df.select(
        "product_name", "energy_100g", "proteins_100g", "fat_100g", "carbohydrates_100g", "countries_en"
    )

    return openfoodfacts_df

def generate_daily_menu(diet_plan, available_products):
    check_diet_compatibility_udf = udf(lambda carbs, proteins, fat, energy: \
                                       (carbs <= diet_plan["carbohydrates_max_g"]) &
                                       (proteins >= diet_plan["protein_min_g"]) &
                                       (fat <= diet_plan["fat_max_g"]) &
                                       (energy <= diet_plan["max_calories_kcal"]), BooleanType())

    filtered_products = available_products.filter(check_diet_compatibility_udf(col("carbohydrates_100g"),
                                                                                col("proteins_100g"),
                                                                                col("fat_100g"),
                                                                                col("energy_100g")))

    daily_menu = filtered_products.sample(withReplacement=False, fraction=0.1).limit(3)

    return daily_menu

def generate_weekly_menu(user_df, diet_plans_df, clean_openfoodfacts_df):
    all_weekly_menus = []

    for user_info in user_df.collect():
        user_id = user_info["user_id"]
        user_diet_plan = user_info["diet_plan"]
        diet_plan_info = diet_plans_df.filter(diet_plans_df["diet_plan"] == user_diet_plan).first()

        weekly_menu = []
        for day in range(1, 8):
            daily_menu = generate_daily_menu(diet_plan_info, clean_openfoodfacts_df)
            daily_menu = daily_menu.withColumn("user_id", lit(user_id)).withColumn("day", lit(day))
            weekly_menu.append(daily_menu)

        all_weekly_menus.append(weekly_menu)

    return all_weekly_menus

def load_data(spark):
    openfoodfacts_df = spark.read.option("delimiter", "\t").csv(OPENFOODFACTS_CSV_PATH, header=True, inferSchema=True)

    diet_plans_df = spark.read.csv(DIET_PLANS_CSV_PATH, header=True, inferSchema=True)

    users_df = spark.read.csv(USERS_CSV_PATH, header=True, inferSchema=True)

    return openfoodfacts_df, diet_plans_df, users_df

def insert_weekly_menus_to_mysql(connection, all_weekly_menus):
    cursor = connection.cursor()
    try:
        for weekly_menu in all_weekly_menus:
            for daily_menu in weekly_menu:
                query = """
                    INSERT INTO weekly_menus (user_id, day, breakfast_product, lunch_product, dinner_product)
                    VALUES (%s, %s, %s, %s, %s)
                """
                data = (
                    daily_menu["user_id"],
                    daily_menu["day"],
                    daily_menu["breakfast_product"],
                    daily_menu["lunch_product"],
                    daily_menu["dinner_product"]
                )
                cursor.execute(query, data)
        connection.commit()
        print("Insertion successful.")
    except mysql.connector.Error as err:
        print("Error during insertion:", err)
    finally:
        cursor.close()

def main():
    spark = SparkSession.builder \
        .appName("OpenFoodFacts ETL") \
        .getOrCreate()

    openfoodfacts_df, diet_plans_df, users_df = load_data(spark)

    clean_openfoodfacts_df = clean_data(openfoodfacts_df, diet_plans_df, users_df)

    all_weekly_menus = generate_weekly_menu(users_df, diet_plans_df, clean_openfoodfacts_df)

    connection = mysql.connector.connect(
        host="localhost",
        user="root",
        password="root",
        database="datawarehouse"
    )
    insert_weekly_menus_to_mysql(connection, all_weekly_menus)
    connection.close()

main()
