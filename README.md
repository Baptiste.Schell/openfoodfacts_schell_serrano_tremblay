# Projet OpenFoodFacts

## Sommaire


[Dépendances](#dépendances) 
**|** [Objectifs du projet](#objectifs-du-projet)
**|** [Collaboration](#collaboration)

## Dépendances

- Python
- Apache Spark
- MySQL

## Objetcifs du projet

Ce projet vise à répondre aux préoccupations croissantes des consommateurs
en proposant des programmes alimentaires adaptés à leurs besoins,
tels que la santé, le sport et le bien-être. 
Pour ce faire, une solution ETL (Extract, Transform, Load)
est mise en place pour collecter des données depuis la source Openfoodfacts.
L'objectif principal est de générer aléatoirement des menus
alimentaires hebdomadaires à partir des informations disponibles 
dans le datalake Openfoodfacts.

Les programmes alimentaires peuvent inclure une variété de régimes 
tels que FODMAP, méditerranéen, DASH, à index glycémique bas,
végétarien, végétalien, régime de la plaque, Paléo, cétogène, etc.

Le projet nécessite la création de deux sources de données supplémentaires : 
une pour les régimes alimentaires avec des seuils pour différentes 
catégories nutritionnelles, et une pour les données des utilisateurs,
chaque utilisateur ayant un régime alimentaire spécifique.

Il est essentiel de garantir la qualité des données en excluant
les produits sans nom, les produits non disponibles dans la région
de l'utilisateur, ainsi que les produits avec des valeurs aberrantes 
pour des composantes nutritionnelles importantes.

Le projet se concentre principalement sur la collecte des données,
leur transformation et leur chargement dans un Datawarehouse pour
un stockage et une consultation ultérieure. La partie visualisation
des données n'est pas incluse dans ce projet et sera abordée dans un 
cours ultérieur.

## Collaboration

Participation du projet par Baptiste Schell, Jérémy Serrano et Enzo Tremblay